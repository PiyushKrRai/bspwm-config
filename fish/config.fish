if status is-interactive
    # Commands to run in interactive sessions can go here
end

alias cls='clear; neofetch --image_size 200'
alias unlock='sudo rm /var/lib/pacman/db.lck'
alias cleanup='sudo pacman -R (pacman -Qtdq)'
alias nano='vim'
alias btstatus='systemctl status bluetooth'
alias btstart='sudo systemctl start bluetooth'
alias update='sudo pacman -Syyu'
alias search='pacman -Ss'
alias install='sudo pacman -S'
alias remove='sudo pacman -R'
alias purge='sudo pacman -Rcns'


cls
